# Grow Shop

Application to manage the day to day activity of the milk shop home delivery system. It allows to create customers, products, configure the milk intake of each customer, allows to regularize the daily customer intake and generates the bill for each customer.

Project directory structure:

* [/client](./client): Vue.js client application ([README](./client/README.md))
* [/server](./server): Node.js server application ([README](./server/README.md))

Demo: https://gitlab.com/patilbhushan/grow-shop/-/blob/main/shop_demo.mp4
