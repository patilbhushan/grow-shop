import { CustomerModel } from '../../src/models/customer.model'
import { expect } from 'chai'

const SAMPLE_CUSTOMER = {
  name: 'Test Customer',
  locality: 'Test locality',
  phone_number: 2345876129,
	email: 'Test@mail.com',
	is_expired: false
}

describe('CustomerModel', () => {
	it('instantiate empty', () => {
		const val = new CustomerModel()
		expect(val.isNew).equal(true)
		expect(val.isValid).equal(false)
	})

	it('instantiate good', () => {
    const val = new CustomerModel(SAMPLE_CUSTOMER)
    expect(val.isNew).equal(true)
    expect(val.isValid).equal(true)
  })

	it('validate name is required', () => {
    const val = new CustomerModel(SAMPLE_CUSTOMER)
    val.name = ''
    expect(val.isValid).equal(false)
  })
})