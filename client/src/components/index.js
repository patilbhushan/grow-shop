import Dialog from '../core/components/Dialog'
import YesNo from '../core/components/YesNo'

export default {
  install: function (Vue) {
    Vue.component('gs-dialog', Dialog),
    Vue.component('gs-yes-no', YesNo)
  }
}
