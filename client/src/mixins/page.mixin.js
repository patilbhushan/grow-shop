import store from '../store'
import debounce from "lodash/debounce"
import { LocalStorageModel } from '../core/storage'
import { PaginationModel } from '../models/pagination.model'
import { FilterModel } from '../models/filter.model'

const ROWS_PER_PAGE_KEY = 'itemsPerPage'
const storage = new LocalStorageModel('pagination')

// keeps pagination in sync between datatables
export default {
  computed: {
    paginationSync: {
      get: function () {
        return this.pagination.toJSON()
      },
      set: function (val) {
        this.pagination = new PaginationModel(val)
        storage.setNumber(ROWS_PER_PAGE_KEY, this.pagination.itemsPerPage)
      }
    }
  },
  methods: {
    updateFilter: debounce(function (filter) {
      this.filter = filter
      this.refreshFilters()
    }, 500),
    
    refreshFilters () {
      if (this.pagination.page === 1) {
        this.refreshPage()
      } else {
        this.pagination.page = 1
      }
    },
  
    async refreshPage () {
      this.isLoading = true
      await this.loadData()
      this.isLoading = false
    },
  
    setBreadcrumbs (...crumbs) {
      store.commit('setBreadCrumbs', crumbs)
    },
    setCustomer(customer) {
      store.commit('setCustomer', customer)
    }
  },
  data () {
    return {
      isLoading: false,
      filter: new FilterModel(),
      pagination: new PaginationModel({ itemsPerPage: storage.getNumber(ROWS_PER_PAGE_KEY) })
    }
  },
  watch: {
    pagination: {
      handler: function () {
        this.refreshPage()
      },
      deep: true
    }
  }
}

