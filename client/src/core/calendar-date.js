const RE_DATE = /(\d{4})-(\d{2})-(\d{2})/
const MONTHS = ['', 'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

export class CalendarDate {
  constructor (value) {
    this.year = 0
    this.month = 0
    this.day = 0
    this.dateSortOrder = 0
    Object.assign(this, getDateParts(value))
  }

  toJSON () {
    return this.isValid ? [this.year, this.month, this.day].join('-') : null
  }

  toString () {
    return this.isValid ? `${MONTHS[this.month]} ${this.day}, ${this.year}` : ''
  }

  get isValid () {
    return this.day > 0 && this.day <= 31 && this.month > 0 && this.month <= 12 && this.year > 0 && this.year <= 9999
  }
}

function getDateParts (value) {
  if (!value) return null
  const match = RE_DATE.exec(value)
  if (match) return { year: parseInt(match[1]), month: parseInt(match[2]), day: parseInt(match[3]), dateSortOrder: parseInt(match[1] + match[2] + match[3]) }
}
