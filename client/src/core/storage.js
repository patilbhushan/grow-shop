export class LocalStorageModel {
  constructor (keyPrefix) {
    this.storeType = 'local'
    this._keyPrefix = keyPrefix + '_'
    this._storage = window[this.storeType + 'Storage']
    if (this._storage === undefined) {
      throw new Error(this.storeType + ' is not available')
    }
  }

  getString (key, defaultValue) {
    const val = this._storage.getItem(this._keyPrefix + key)
    return val || defaultValue
  }

  getObject (key, defaultValue) {
    const strValue = this.getString(key)
    return !strValue ? defaultValue : JSON.parse(strValue)
  }

  getNumber (key, defaultValue) {
    const strValue = this.getString(key)
    const numValue = parseFloat(strValue)
    return isNaN(numValue) ? defaultValue : numValue
  }

  setString (key, value) {
    this._storage.setItem(this._keyPrefix + key, value)
  }

  setObject (key, value) {
    this.setString(key, JSON.stringify(value))
  }

  setNumber (key, value) {
    this.setString(key, value.toString())
  }

}
