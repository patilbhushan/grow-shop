import Vue from 'vue'
import Vuex from 'vuex'
import { BreadcrumbModel } from '../models/breadcrumb.model'
import { CustomerModel } from '../models/customer.model'

Vue.use(Vuex)

const store = new Vuex.Store({
  state: {
    breadcrumbs: [],
    alert: { show: false, color: 'info', message: '' },
    customer: new CustomerModel()
  },
  mutations: {
    setBreadCrumbs (state, items) {
			if (!Array.isArray(items)) items = [items]
			state.breadcrumbs = items.map(item => {
				return item instanceof BreadcrumbModel ? item : new BreadcrumbModel(item)
			})
    },
    setAlert (state, data) {
      Object.assign(state.alert, { color: 'info', show: true }, data)
      state.alert = data
    },
    setCustomer(state, data) {
      state.customer = data
    }
  }
})

export default store
