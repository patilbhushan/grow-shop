import Vue from 'vue'
import Router from 'vue-router'
import Home from './components/Home/Home.vue'
import Customer from './components/Customers/Customer.vue'
import Customers from './components/Customers/Customers.vue'
import CustomerDetail from './components/Customers/CustomerDetail.vue'
import CustomerConfiguration from './components/Customers/CustomerConfiguration.vue'
import Products from './components/Products/Products.vue'
import PageNotFound from '../src/core/components/PageNotFound.vue'

Vue.use(Router)

const router = new Router({
	routes:[
		{ path: '/', name: 'home', component: Home },
		{ path: '/customers', name: 'customers', component: Customers },
		{
			path: '/customer/:id',
			name: 'customer',
			component: Customer,
			props: true,
			children: [
				{ path: 'detail', name: 'customer.detail', component: CustomerDetail, props: true },
				{ path: 'configuration', name: 'customer.configuration', component: CustomerConfiguration, props: true }
			]
			},
		{ path: '/products', name: 'products', component: Products },
		{ path: '*', component: PageNotFound }
	]
})

export default router
