export class BreadcrumbModel {
  constructor (data) {
    if (typeof data === 'string') {
      this.title = data
    } else if (Array.isArray(data)) {
      this.title = data[0]
      this.href = data[1]
    } else if (data === true) {
      this.isLoading = true
    } else {
      const d = data || {}
      this.title = d.title
      this.href = d.href
      this.isLoading = !!d.isLoading
    }
  }
}
