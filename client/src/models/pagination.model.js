export class PaginationModel {
  constructor (data) {
    const d = data || {}
    this.descending = (d.sortDesc && d.sortDesc[0]) || false
    this.page = isNaN(d.page) ? 1 : Math.max(d.page, 1)
    this.itemsPerPage = isNaN(d.itemsPerPage) ? 10 : Math.min(d.itemsPerPage, 500)
    this.sortBy = d.sortBy || []
  }

  toJSON () {
    return {
      descending: this.descending,
      page: this.page,
      itemsPerPage: this.itemsPerPage,
      sortBy: this.sortBy
    }
  }
}
