import { validators, nonEmptyString } from "./model-helper"
import { BaseModel } from "./_base.model"

export class CustomerModel extends BaseModel {
    constructor(data) {
			super(data)
			
			const d = data || {}

			this.name = d.name
			this.locality = d.locality
			this.phone_number = d.phone_number
			this.email = d.email
			this.is_expired = d.is_expired || false
    }

		get isNew () {
        return this.id === null || this.id === undefined
    }

    get isValid () {
        return nonEmptyString(this.name) && nonEmptyString(this.locality)
    }

    static getValidators () {
        return {
            name: [validators.required('Name')],
            locality: [validators.required('Locality')],
            phone_number: [validators.isNumber('PhoneNumber')],
            email: [validators.isValidEmail('Email')]
        }
    }

  toJSON () {
    return {
      id: this.id || undefined,
			name: this.name,
			locality: this.locality,
			phone_number: this.phone_number || undefined,
			email: this.email,
			is_expired: this.is_expired || false
    }
  }
}