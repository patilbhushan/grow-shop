import { validators, hasValue } from "./model-helper"
import { BaseModel } from "./_base.model"
import { CalendarDate } from '../core/calendar-date'

export class ProductRateModel extends BaseModel {
    constructor(data, meta) {
			super(data)
			
			const d = data || {}

			this.product_id = d.product_id
      this.rate = d.rate
			this.effective_from = new CalendarDate(d.effective_from)
      this.effective_to = new CalendarDate(d.effective_to || '9999-12-31')
      this.is_expired = d.effective_to === '9999-12-31'
      this.name = ''
      
      if (meta && meta.data && Array.isArray(meta.data)) {
        const product = meta.data.find(({id}) => id === d.product_id);
        this.name = product ? product.name : ''
      }
    }

		get isNew () {
        return this.id === null || this.id === undefined
    }

    get isProductRate () {
      return true
    }

    get isValid () {
        return hasValue(this.rate) && !isNaN(this.rate) && this.effective_from.isValid
    }

    static getValidators () {
        return {
            rate: [validators.required('Rate')],
            effective_from: [validators.required('EffectiveFrom')],
        }
    }

  toJSON () {
    return {
      id: this.id || undefined,
			product_id: this.product_id,
			rate: this.rate,
			effective_from: this.effective_from,
			effective_to: this.effective_to			
    }
  }
}