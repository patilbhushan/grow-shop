export const validators = {
    required (fieldName) {
        return value => !!value || `${fieldName} is required.`
    },
    isNumber (fieldName) {
        return value => !isNaN(value) || `${fieldName} must be a number.`
    },
    nonEmpty (fieldName) {
        return value => nonEmptyString(value) || `${fieldName} must not be empty`
    },
    isValidEmail() {
        return value => !nonEmptyString(value) || isEmail(value) || 'Email is invalid'
    }    
}

export function nonEmptyString (value) {
    return typeof value === 'string' && value.trim().length > 0
}

export function isEmail (value) {
    const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return value && pattern.test(value)
}

export function hasValue (value) {
    return value !== null && value !== undefined && value !== ''
}
