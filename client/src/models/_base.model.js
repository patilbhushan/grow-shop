import cloneDeep from 'lodash/cloneDeep'

export class BaseModel {
  constructor (data) {
    const d = data || {}
    this.id = d.id
  }

  makeCopy () {
    return cloneDeep(this)
  }  
}
