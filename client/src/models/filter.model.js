import { hasValue } from './model-helper'
import Vue from 'vue'

function hasFilterValue (value) {
  if (Array.isArray(value)) return value.length > 0
  return hasValue(value)
}

export class FilterModel {
  constructor (data) {
    const d = data || {}

    this.values = {} // actual filter values by field name
    this.fields = {} // true/false map of whether field is active or not

    Object.entries(d).forEach(([key, value]) => {
      this.values[key] = value
      this.fields[key] = hasFilterValue(value)
    })

    if (this.fields.country) this.fields.state = true
  }

  get isFiltering () {
    return Object.entries(this.values)
      .some(([key, value]) => this.fields[key] && (hasFilterValue(value)))
  }

  toggleField (key) {
    return this.setFieldActive(key, !this.fields[key])
  }

  setFieldActive (key, isActive) {
    Vue.set(this.fields, key, !!isActive)
    if (this.values[key] === undefined) Vue.set(this.values, key, null)
    return hasFilterValue(this.values[key]) // determines if toggling this field makes a difference
  }

  clearFields () {
    let changed = false
    Object.keys(this.fields).forEach(key => {
      changed = changed || (this.fields[key] && hasFilterValue(this.values[key]))
      this.fields[key] = false
      this.values[key] = null
    })
    
    return changed
  }

  toJSON () {
    const output = {}
    Object.entries(this.values).forEach(([key, value]) => {
      if (this.fields[key]) {
        if (hasFilterValue(value)) output[key] = value
      }
    })
    return output
  }

  toParameters () {
    const output = {}
    Object.entries(this.values).forEach(([key, value]) => {
      if (this.fields[key]) {
        if (hasFilterValue(value)) {
          if (typeof value === 'object') {
            output[key] = value.id
          } else {
            output[key] = value
          }
        }
      }
    })
    return output
  }
}
