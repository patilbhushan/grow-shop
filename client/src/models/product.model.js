import { validators, nonEmptyString } from "./model-helper"
import { BaseModel } from "./_base.model"
import { ProductRateModel } from "./product_rate.model"

export class ProductModel extends BaseModel {
    constructor(data, meta) {
			super(data)
			
			const d = data || {}

			this.name = d.name
      this.products_rate = d.products_rate ? d.products_rate.map(item => (new ProductRateModel(item, meta))) : []
			this.is_expired = d.is_expired || false      
    }

		get isNew () {
        return this.id === null || this.id === undefined
    }

    get isValid () {
        return nonEmptyString(this.name)
    }

    get isProductRate () {
      return false
    }

    static getValidators () {
        return {
            name: [validators.required('Name')],
            rate: [validators.required('Rate')],
            effective_from: [validators.required('EffectiveFrom')],
            effective_to: [validators.required('EffectiveTo')]
        }
    }

  toJSON () {
    return {
      id: this.id || undefined,
			name: this.name,
			is_expired: this.is_expired || false,
      products_rate: this.products_rate
    }
  }
}