import BaseDataService from './_base-data.service'
import { ProductRateModel } from '../models/product_rate.model'

export class ProductRateService extends BaseDataService {
  saveProductRate (product) {
    return this.save('products_rate', product, {}, ProductRateModel)
  }
  
  removeProductRate (product) {
    return this.remove(`products_rate/${product.id}`, null, ProductRateModel)
  }
}

const service = new ProductRateService()
export default service
