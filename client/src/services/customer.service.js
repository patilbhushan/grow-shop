import BaseDataService from './_base-data.service'
import { CustomerModel } from '../models/customer.model'

export class CustomerService extends BaseDataService {
  getAllCustomers (pagination, filter) {
    return this.getEntityPage('customers', CustomerModel, pagination, filter)
  }
  saveCustomer (customer) {
    return this.save('customers', customer, {}, CustomerModel)
  }
  getCustomer (id) {
    return this.fetch(`customers/${id}`, null, CustomerModel)
  }
}

const service = new CustomerService()
export default service
