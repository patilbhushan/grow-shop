import BaseDataService from './_base-data.service'
import { ProductModel } from '../models/product.model'

export class ProductService extends BaseDataService {
  getAllProducts (pagination, filter) {
    return this.getEntityPage('products', ProductModel, pagination, filter)
  }
  saveProduct (product) {
    return this.save('products', product, {}, ProductModel)
  }
  getProduct (id) {
    return this.fetch(`products/${id}`, null, ProductModel)
  }
  removeProduct (product) {
    return this.remove(`products/${product.id}`, null, ProductModel)
  }
}

const service = new ProductService()
export default service
