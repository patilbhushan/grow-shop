import axios from 'axios'
import { PaginationModel } from '../models/pagination.model'
import { FilterModel } from '../models/filter.model'

// https://www.npmjs.com/package/axios#example
export default class BaseDataService {
  constructor () {
    this.http = axios.create({ baseURL: '/api/' })
    this.cancelToken = axios.CancelToken
    this.source = undefined
  }

  fixParameters (pagination = {}, filter = {}) {
    // convert Vuetify pagination params (itemsPerPage, page, sortBy) to API params (limit, start, order)
    if (pagination instanceof PaginationModel) pagination = pagination.toJSON()
    if (filter instanceof FilterModel) filter = filter.toParameters()
        
    const params = {
      ...{
        start: 0,
        limit: pagination.itemsPerPage || pagination.limit || 15
      },
      ...filter
    }

    if (params.limit === -1) params.limit = 1000
    if (pagination.page > 0) params.start = (pagination.page - 1) * params.limit
    if (pagination.sortBy.length > 0) params.order = (pagination.descending ? '-' : '') + pagination.sortBy

    return params
  }

  async getEntityPage (url, DataItemType, pagination, filter) {
    await this.makeCancelTokenSource()
    const params = { ...this.fixParameters(pagination, filter) }
    return this.fetch(url, params, DataItemType, this.source.token)
  }

  makeCancelTokenSource () {
    if (this.source) this.source.cancel('Request Canceled: User filter change or locally stored filters')
    this.source = undefined
    this.source = this.cancelToken.source()
  }

  fetch (url, params, DataItemType, cancelToken) {
    const config = { params }
    if (this.source && cancelToken) config.cancelToken = cancelToken
    
    return this.http.get(url, config)
      .then(response => fixResponse(response, DataItemType))
  }

  save (url, data, params, DataItemType) {
    const method = data.isNew ? 'post' : 'put'
    const key = data.id || data.code
    if (key && !data.isNew) url += '/' + key

    return this.http({ url, method, data, params })
      .then(response => fixResponse(response, DataItemType))
  }

  remove (url, params, DataItemType) {
    return this.http({ url, method: 'delete', params })
      .then(response => fixResponse(response, DataItemType))
  }
}

function modelFactory (ItemType, data, meta) {
  if (Array.isArray(data)) {
    return data.map(datum => new ItemType(datum, meta))
  } else {
    return new ItemType(data, meta)
  }
}

function fixResponse (response, DataItemType) {
  const body = response.data || {}
  if (body.data && DataItemType) {
    body.data = modelFactory(DataItemType, body.data, body)
  }
  return body
}
