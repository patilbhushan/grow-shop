import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify'
import components from './components'
import router from './router'
import store from './store'

Vue.use(components)

new Vue({
  router,
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
