# Grow Shop Client

This project is meant to help in developing a [VueJS](https://vuejs.org) client application.

## Directory Structure
This project was originally generated with [the Vue CLI](https://github.com/vuejs/vue-cli)
like this: `vue create`

* [src](./src): source of the client application
  - [src/core](./src/core): shared components between sub-applications
  - src/____: sub-application directories here
* [tests](./tests): standard unit tests

## Prerequisites

* [NodeJS](https://nodejs.org) - JS outside the browser
* [Yarn](https://yarnpkg.com/en/) - deterministic package manager

## Development

To begin, open two command prompt windows.
In one, start up your [local server](../server), and in the second, the other [local client](../client)
using following command:
```
yarn start
```

These are some of the frameworks, libraries, and tools used in this project:

* [Vue.js 2](https://vuejs.org): the framework
* [vue-router 2](https://router.vuejs.org/en/): Vue's recommended router
* [Vuetify.js](https://vuetifyjs.com): material component framework for Vue
* [axios](https://github.com/mzabriskie/axios): promise-based HTTP client
* [Pug](https://pugjs.org/) - HTML preprocessor

