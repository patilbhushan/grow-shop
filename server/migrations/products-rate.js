'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";').then(async () => {
      await queryInterface.createTable('products_rate', {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('uuid_generate_v4()')
        },
        product_id: {
          type: Sequelize.UUID,
          allowNull: false,
          foreignKey: true,
          references: {
            model: 'products',
            key: 'id'
          }
        },
        rate: {
          type: Sequelize.FLOAT,
          allowNull: false
        },
        effective_from: {
          type: Sequelize.DATEONLY,
          allowNull: false
        },
        effective_to: {
          type: Sequelize.DATEONLY,
          allowNull: false
        },
        created_date: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
        },
        modified_date: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
        }
      })
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('products_rate')
  }
}
