'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";').then(async () => {
      await queryInterface.createTable('products', {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('uuid_generate_v4()')
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true
        },
        is_expired: {
          type: Sequelize.BOOLEAN,
          allowNull: false
        },
        created_date: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
        },
        modified_date: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
        }
      })
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('products')
  }
}
