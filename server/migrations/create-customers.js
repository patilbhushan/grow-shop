'use strict'
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.sequelize.query('CREATE EXTENSION IF NOT EXISTS "uuid-ossp";').then(async () => {
      await queryInterface.createTable('customers', {
        id: {
          allowNull: false,
          primaryKey: true,
          type: Sequelize.UUID,
          defaultValue: Sequelize.literal('uuid_generate_v4()')
        },
        name: {
          type: Sequelize.STRING,
          allowNull: false,
          unique: true
        },
        locality: {
          type: Sequelize.STRING,
          allowNull: false
        },
        phone_number: {
          type: Sequelize.BIGINT
        },
        email: {
          type: Sequelize.STRING
        },
        is_expired: {
          type: Sequelize.BOOLEAN,
          allowNull: false,
          defaultValue: false
        },
        created_date: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
        },
        modified_date: {
          allowNull: false,
          type: Sequelize.DATE,
          defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
        }
      })
    })
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('customers')
  }
}
