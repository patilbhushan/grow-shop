const express = require('express')

const app = express()
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use('/api', require('./api'))

// enable no-extension request in root to map to .html file
app.get(/\/[^./]+$/, function (req, res, next) {
  req.url += '.html'
  next('route')
})

app.use(express.static('public'))

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found')
  err.status = 404
  next(err)
})

// error handler
app.use(function (err, req, res, next) {
  res.status(err.status || 500)
  const re = new RegExp(__dirname, 'g')
  const stack = (err.stack + '').replace(re, '').split('\n')
  res.send({ message: err.message || 'internal error', errors: err.errors, stack })
})

module.exports = app
