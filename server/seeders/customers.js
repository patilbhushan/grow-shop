'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    const customers = []
    const names = ['Laxminarayan No.2', 'Ogale', 'Lalit Rathi', 'Sahu C-30 Rathi', 'Dongaonkar Sai No. 8', 'Soienda', 'Lalit Adhane', 'Reddy Meridian No. 8 ', 'Gaikwad', 'Gavhane', 'Suresh Jadhav', 'Shree Kripa No. 6', 'Dr. Borde', 'Nandkhedkar', 'Dr. Uday Kulkarni', 'Green No. 7', 'Mrs. Bhalerao Madem', 'Ashok Karva', 'Pratap Dhopte', 'Kulkarni RB- 6', 'Vipradas', 'Dashrate No 7', 'Kingaonkar', 'Rathod Green 12', 'Kulkarni No. 6 Rathi Apart.', 'Bhosekar D-7', 'Somani D-13', 'Bedekar', 'Dr. Bonde', 'Pathak Mitra Mandal', 'Bhatiya New Green Apt.', 'Sakar Shishu Sangopan Centre', 'Patil above Chauble', 'Dodle C-17 Rathi', 'Kandilkar', 'Adv. Bhatiya old Green Apt.', 'Mohan Savant', 'Chauble', 'Mahajan', 'Jadhav Madam', 'Bansod', 'Dharmesh Sampat', 'Roonwal', 'Narkhade', 'Kashikar', 'Vitekar', 'Kendrekar', 'Adv. Deshpande', 'Baghve', 'S.S. Jaggi', 'Dr. Bharati Patil', 'Aedadekar Disha 5', 'Sharma Disha', 'Dr. Kulkarni Heritage No.2 ', 'Deshpande Srirang Shejari', 'Deepak Joshi', 'SamsherSingh Sodhi', 'Sodhi Upper', 'Sodhi Lower', 'RajTara I', 'RajTara II', 'Arun Bhole', 'Bakale', 'Bhattachrya B-9', 'Mughalwadkar', 'Pantoji', 'Bakula Deshpande', 'Pradip Pargaonkar', 'Adv.Dhorde Patil', 'Suresh Ballal', 'D.D.Patil', 'Mahajan B-1 Abhirika', 'Lahariya']
    const locality = ['Jyotinagar', 'Dashmeshnagar', 'Sahakarnagar', 'Osmanpura', 'Jawahar Colony']
    for (let i = 0; i < 73; i++) {
      customers.push({
        name: names[i],
        locality: locality[Math.floor(Math.random() * locality.length)],
        phone_number: 1000000000 + Math.floor(Math.random() * 9000000000),
        email: names[i] + '@test.com'
      })
    }
    await queryInterface.bulkInsert('customers', customers, {})
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('customers', null, {})
  }
}
