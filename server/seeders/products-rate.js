'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const items = []
    items.push({
      product_id: 'f6d346da-376a-441b-9eb2-68306a16490d',
      rate: 24,
      effective_from: '2020-10-01',
      effective_to: '9999-12-31'
    })

    await queryInterface.bulkInsert('products_rate', items, {})
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('products_rate', null, {})
  }
}
