'use strict'

module.exports = {
  up: async (queryInterface, Sequelize) => {
    const items = []
    items.push({
      name: 'Vikas Cow Milk',
      rate: 24,
      effective_from: '2020-10-01',
      effective_to: '9999-12-31',
      is_expired: false
    })

    await queryInterface.bulkInsert('products', items, {})
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.bulkDelete('products', null, {})
  }
}
