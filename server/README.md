# Grow Shop Server

This is an [Express](http://expressjs.com) application that interacts with a PostgreSQL database.

## Directory Structure

* [api](./api): API route handlers, named basically corresponding to their path
* [models](./models): data models for querying and storing data to the DB, named roughly for tables
* [lib](./lib): helpers
* [migrations](./migrations): migrate the data models schema to postgres db
* [seeders](./seeders): Push data into db for testing
* [test](./test): unit tests

## Prerequisites

* [NodeJS](https://nodejs.org) - JS on the server
* [Yarn](https://yarnpkg.com/en/) - deterministic package manager

## Development

This project adheres to the [JavaScript Standard Style](https://standardjs.com).
For local development, update the config/config.json file with db credentials.
The very first time you start this project, run the following commands to install libraries and initialize:
```
yarn install
yarn start
```

If you start your local server with `yarn start`, it will watch the directory for changes and restart automatically.

These are some of the libraries used in this project:

* [Express](http://expressjs.com) - Node web framework
* [Mocha](https://mochajs.org) - test runner
* [Sequelize](http://docs.sequelizejs.com/en/v3/) - ORM for PostgreSQL
* [SequelizeCLI](https://sequelize.org/v3/search.html?q=CLI) - To generate config, models, migration and seeders.
* [Lodash](https://lodash.com) - JavaScript utility library
* [PM2](http://pm2.keymetrics.io) - keep the Node process alive and use multiple cores.
