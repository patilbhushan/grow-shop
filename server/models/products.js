const helper = require('../lib/model-helper')
const { Model } = require('sequelize')
const { queryToOptions } = require('../lib/query-helper')

module.exports = (sequelize, DataTypes) => {
  class products extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      products.hasMany(models.products_rate, { as: 'products_rate', foreignKey: 'product_id', targetKey: 'id' })
    }

    toJSON () {
      return {
        ...this.get()
      }
    }
  };
  products.init({
    id: {
      type: DataTypes.UUIDV4,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    is_expired: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'products',
    timestamps: false,
    freezeTableName: true
  })

  products.getMany = (query) => {
    const opt = {
      order: [['name']],
      ...queryToOptions(query),
      include: [{ model: sequelize.models.products_rate, as: 'products_rate' }]
    }
    return products.findAndCountAll(opt)
  }

  products.createProduct = (options) => {
    return products.create(options)
      .then(result => {
        return result
      })
      .catch(err => {
        helper.makeErrorHandler('Product')(err)
      })
  }

  products.prototype.updateByUser = function (item) {
    return this.update(item)
      .then(result => {
        return result
      })
      .catch(err => {
        helper.makeErrorHandler('Product')(err)
      })
  }

  products.prototype.remove = async function (id) {
    await this.destroy()
    return this
  }

  return products
}
