const { Model } = require('sequelize')
const helper = require('../lib/model-helper')

module.exports = (sequelize, DataTypes) => {
  class productsRate extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      productsRate.belongsTo(models.products, { as: 'products', foreignKey: 'product_id', targetKey: 'id' })
    }

    toJSON () {
      return {
        ...this.get()
      }
    }
  };
  productsRate.init({
    id: {
      type: DataTypes.UUIDV4,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    product_id: {
      type: DataTypes.UUIDV4,
      allowNull: false,
      foreignKey: true
    },
    rate: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    effective_from: {
      type: DataTypes.DATEONLY,
      allowNull: false
    },
    effective_to: {
      type: DataTypes.DATEONLY,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'products_rate',
    timestamps: false,
    freezeTableName: true
  })

  productsRate.createProductRate = async (options) => {
    await updateOldRecord(options) // update the effective_to date of active record before adding a new one
    return productsRate.create(options)
      .then(result => {
        return result
      })
      .catch(err => {
        helper.makeErrorHandler('productsRate')(err)
      })
  }

  productsRate.prototype.updateByUser = async function (item) {
    return this.update(item)
      .then(result => {
        return result
      })
      .catch(err => {
        helper.makeErrorHandler('productsRate')(err)
      })
  }

  productsRate.prototype.remove = async function (id) {
    await this.destroy()
    return this
  }

  async function updateOldRecord (item) {
    const oldSQL = "UPDATE products_rate SET effective_to=? WHERE product_id=? AND effective_to='9999-12-31'"
    const enddate = new Date(item.effective_from)
    enddate.setDate(enddate.getDate() - 1)
    const replacements = [enddate, item.product_id]
    await sequelize.query(oldSQL, { replacements })
  }

  return productsRate
}
