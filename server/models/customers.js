const helper = require('../lib/model-helper')
const { Model } = require('sequelize')
const { queryToOptions } = require('../lib/query-helper')

module.exports = (sequelize, DataTypes) => {
  class customers extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate (models) {
      // define association here
    }

    toJSON () {
      return {
        ...this.get()
      }
    }
  };

  customers.init({
    id: {
      type: DataTypes.UUIDV4,
      defaultValue: DataTypes.UUIDV4,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    locality: {
      type: DataTypes.STRING,
      allowNull: false
    },
    phone_number: {
      type: DataTypes.BIGINT
    },
    email: {
      type: DataTypes.STRING
    },
    is_expired: {
      type: DataTypes.BOOLEAN,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'customers',
    timestamps: false
  })

  customers.getMany = (query) => {
    const opt = {
      order: [['name']],
      ...queryToOptions(query)
    }
    return customers.findAndCountAll(opt)
  }

  customers.createCustomer = (options) => {
    return customers.create(options)
      .then(result => {
        return result
      })
      .catch(err => {
        helper.makeErrorHandler('Customer')(err)
      })
  }

  customers.prototype.updateByUser = function (item) {
    return this.update(item)
      .then(result => {
        return result
      })
      .catch(err => {
        helper.makeErrorHandler('Customer')(err)
      })
  }

  customers.prototype.remove = function (id) {
    return this.update({ is_expired: true, id: id })
  }

  return customers
}
