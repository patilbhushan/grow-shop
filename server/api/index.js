const express = require('express')
const router = express.Router()

router.use('/customers', require('./customers'))
router.use('/products', require('./products'))
router.use('/products_rate', require('./products_rate'))

module.exports = router
