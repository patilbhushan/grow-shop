const express = require('express')
const errors = require('../lib/errors')
const models = require('../models')
const model = models.products
const DATA_TYPE = 'product'

const router = express.Router()
module.exports = router

router.get('/', getAll)
router.post('/', createOne)
router.get('/:id', getOne)
router.put('/:id', updateOne)
router.delete('/:id', removeOne)

// helpers --------------------------------------------------------------------

async function findAndVerify (id) {
  const item = await model.findOne({ where: { id: id } })
  if (!item) throw new errors.NotFoundError(id, DATA_TYPE)
  return item
}

function itemsToOutput (dataType, res, items, total) {
  const output = { dataType, total }
  if (items) {
    if (Array.isArray(items)) {
      output.data = items.map(function (item) {
        return item.toJSON ? item.toJSON() : item
      })
    } else {
      output.data = items.toJSON ? items.toJSON() : items
    }
  }
  res.json(output)
}
// ----------------------------------------------------------------------------

async function getAll (req, res, next) {
  try {
    const result = await model.getMany(req.query)
    itemsToOutput(DATA_TYPE, res, result.rows, result.count)
  } catch (err) {
    next(err)
  }
}

function createOne (req, res, next) {
  model.createProduct(req.body)
    .then(created => { itemsToOutput(DATA_TYPE, res, created) })
    .catch(next)
}

function getOne (req, res, next) {
  findAndVerify(req.params.id)
    .then(item => { itemsToOutput(DATA_TYPE, res, item) })
    .catch(next)
}

function updateOne (req, res, next) {
  findAndVerify(req.params.id)
    .then(item => item.updateByUser(req.body))
    .then(updated => { itemsToOutput(DATA_TYPE, res, updated) })
    .catch(next)
}

function removeOne (req, res, next) {
  findAndVerify(req.params.id)
    .then(product => product.remove(req.params.id)) // Validation for deletion should be handled by foreign key relationships
    .then(deleted => { itemsToOutput(DATA_TYPE, res, deleted) })
    .catch(err => {
      if (err.name === 'SequelizeForeignKeyConstraintError') return next(new errors.ValidationError('Cannot delete Product that is linked to rate or customer'))
      return next(err)
    })
}
