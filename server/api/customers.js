const express = require('express')
const errors = require('../lib/errors')
const models = require('../models')
const model = models.customers
const DATA_TYPE = 'customer'

const router = express.Router()
module.exports = router

router.get('/', getAll)
router.post('/', createOne)
router.get('/:id', getOne)
router.put('/:id', updateOne)

// helpers --------------------------------------------------------------------

async function findAndVerify (id) {
  const item = await model.findOne({ where: { id: id } })
  if (!item) throw new errors.NotFoundError(id, DATA_TYPE)
  return item
}

function itemsToOutput (dataType, res, items, total) {
  const output = { dataType, total }
  if (items) {
    if (Array.isArray(items)) {
      output.data = items.map(function (item) {
        return item.toJSON ? item.toJSON() : item
      })
    } else {
      output.data = items.toJSON ? items.toJSON() : items
    }
  }
  res.json(output)
}
// ----------------------------------------------------------------------------

async function getAll (req, res, next) {
  try {
    const result = await model.getMany(req.query)
    itemsToOutput(DATA_TYPE, res, result.rows, result.count)
  } catch (err) {
    next(err)
  }
}

function createOne (req, res, next) {
  model.createCustomer(req.body)
    .then(created => { itemsToOutput(DATA_TYPE, res, created) })
    .catch(next)
}

function getOne (req, res, next) {
  findAndVerify(req.params.id)
    .then(item => { itemsToOutput(DATA_TYPE, res, item) })
    .catch(next)
}

function updateOne (req, res, next) {
  if (req.body.is_expired) {
    findAndVerify(req.params.id)
      .then(customer => customer.remove(req.params.id))
      .then(archived => { itemsToOutput(DATA_TYPE, res, archived) })
      .catch(next)
  } else {
    findAndVerify(req.params.id)
      .then(item => item.updateByUser(req.body))
      .then(updated => { itemsToOutput(DATA_TYPE, res, updated) })
      .catch(next)
  }
}
