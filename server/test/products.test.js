/* eslint-env mocha */
const expect = require('chai').expect
const helper = require('./test-helper')
let product

describe('Sample', () => {
  it('Testing the test. Expect 1 to be 1!', () => {
    expect(1).to.equal(1)
  })
})

describe('/api/products', () => {
  it('Creates a new Product', done => {
    const input = { name: 'Product for testing', is_expired: false }
    helper.makeRequest('post', '/api/products', input)
      .end((err, res) => {
        expect(res.status).to.equal(200)
        product = res.body.data
        expect(product.name).to.equal(input.name)
        expect(product.is_expired).to.equal(input.is_expired)
        done(err)
      })
  })

  it('Fails to create invalid Product', done => {
    const invalidProduct = { is_expired: false }
    helper.makeRequest('post', '/api/products', invalidProduct)
      .end(function (err, res) {
        expect(res.status).to.equal(400)
        expect(res.body.message).to.contain('name')
        done(err)
      })
  })

  it('Delete a Product', done => {
    helper.makeRequest('delete', '/api/products/' + product.id)
      .end(function (err, res) {
        expect(res.status).to.equal(200)
        done(err)
      })
  })
})
