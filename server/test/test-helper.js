const app = require('../app')
const chai = require('chai')
const chaiHttp = require('chai-http')

chai.use(chaiHttp)

module.exports = {
  makeRequest
}

function makeRequest (method, url, input, contentType = 'application/json') {
  return chai.request(app)[method](url)
    .set('Content-Type', contentType)
    .send(input)
}
