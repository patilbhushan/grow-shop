const errors = require('./errors')

module.exports = {
  makeErrorHandler
}

function makeErrorHandler (entity) {
  return function errorHandler (err) {
    if (err.name === 'SequelizeUniqueConstraintError') {
      throw new errors.DuplicateError(entity)
    } else {
      throw new errors.ValidationError(err.message)
    }
  }
}
