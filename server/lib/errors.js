module.exports = {
  NotFoundError,
  ValidationError,
  DuplicateError
}

function NotFoundError (id, name) {
  this.status = 404
  this.message = (name || 'item') + ' not found'
  this.id = id
}

function ValidationError (message, field) {
  this.status = 400 // bad request
  this.field = field
  this.message = message || 'Validation error'
}

function DuplicateError (entity) {
  this.status = 400 // bad request
  this.field = entity
  this.message = entity ? entity + ' must be unique' : 'Duplicate Error. Something is not unique'
}
