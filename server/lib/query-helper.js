const _ = require('lodash')
const { Op } = require('sequelize')

module.exports = {
  queryToOptions
}

function fixInt (value, upper, defaultValue) {
  const int = parseInt(value)
  return isNaN(int) ? defaultValue : _.clamp(int, 0, upper)
}

// convert URL query to model options
function queryToOptions (query = {}) {
  const options = {
    limit: fixInt(query.limit, 500, 100),
    offset: fixInt(query.start, 500, 0),
    where: {}
  }

  const filterQuery = _.omit(query, ['start', 'limit', 'order'])
  _.forEach(filterQuery, (value, key) => {
    options.where[key] = value
  })

  if (query.order) {
    const direction = query.order[0] === '-' ? 'DESC' : 'ASC'
    const field = direction === 'DESC' ? query.order.substr(1) : query.order
    options.order = [[field, direction]]
  }

  if (query.name) options.where.name = termToLike(query.name)

  if (query.locality) options.where.locality = termToLike(query.locality)

  return options
}

function termToLike (term) {
  const words = _.words(term)
  if (words.length < 1) return

  const conditions = words.map(word => ({ [Op.iLike]: '%' + word + '%' }))
  return conditions.length === 1 ? conditions[0] : { [Op.and]: conditions }
}
