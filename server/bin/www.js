const { sequelize } = require('../models')

startServer()

process.on('SIGINT', onExit)

function startServer () {
  const app = require('../app')

  app.set('port', process.env.PORT || 3000)

  const server = app.listen(app.get('port'), async () => {
    console.log('Express server listening on port ' + server.address().port)
    await sequelize.authenticate()
    console.log('Database is connected!!')
  })
}

function onExit () {
  const models = require('../models')
  models.sequelize.close()
  process.exit(0)
}
